/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package com.example.android.unscramble.ui.game

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import com.example.android.unscramble.R
import com.example.android.unscramble.databinding.GameFragmentBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

/**
 * Fragment where the game is played, contains the game logic.
 * Fragment où le jeu est joué, contient la logique du jeu.
 */
class GameFragment : Fragment() {

    // Binding object instance with access to the views in the game_fragment.xml layout
    // Instance d'objet de liaison avec accès aux vues dans le layout game_fragment.xml
    private lateinit var binding: GameFragmentBinding

    // Create a ViewModel the first time the fragment is created.
    // Créez un ViewModel la première fois que le fragment est créé.

    // If the fragment is re-created, it receives the same GameViewModel instance created by the
    // Si le fragment est recréé, il reçoit la même instance GameViewModel créée par le

    // first fragment.
    // premier fragment.
    private val viewModel: GameViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout XML file and return a binding object instance
        // Gonfle le fichier XML de mise en page et renvoie une instance d'objet de liaison
        binding = DataBindingUtil.inflate(inflater, R.layout.game_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Set the viewModel for data binding - this allows the bound layout access
        // Définir le viewModel pour la liaison de données - cela permet l'accès à la mise en page liée

        // to all the data in the VieWModel
        // à toutes les données du ViewWModel
        binding.gameViewModel = viewModel
        binding.maxNoOfWords = MAX_NO_OF_WORDS
        // Specify the fragment view as the lifecycle owner of the binding.
        // Spécifiez la vue fragmentée en tant que propriétaire du cycle de vie de la liaison.

        // This is used so that the binding can observe LiveData updates
        // Ceci est utilisé pour que la liaison puisse observer les mises à jour LiveData
        binding.lifecycleOwner = viewLifecycleOwner

        // Setup a click listener for the Submit and Skip buttons.
        // Configurez un écouteur de clic pour les boutons Soumettre et Ignorer.
        binding.submit.setOnClickListener { onSubmitWord() }
        binding.skip.setOnClickListener { onSkipWord() }
    }

    /*
    * Checks the user's word, and updates the score accordingly.
    * Vérifie la parole de l'utilisateur et met à jour le score en conséquence.

    * Displays the next scrambled word.
    * Affiche le mot brouillé suivant.

    * After the last word, the user is shown a Dialog with the final score.
    *  Après le dernier mot, l'utilisateur voit apparaître une boîte de dialogue avec le score final.
    */
    private fun onSubmitWord() {
        val playerWord = binding.textInputEditText.text.toString()

        if (viewModel.isUserWordCorrect(playerWord)) {
            setErrorTextField(false)
            if (!viewModel.nextWord()) {
                showFinalScoreDialog()
            }
        } else {
            setErrorTextField(true)
        }
    }

    /*
     * Skips the current word without changing the score.
     * Saute le mot actuel sans changer le score.

     * Increases the word count.
     * Augmente le nombre de mots.

     * After the last word, the user is shown a Dialog with the final score.
     * Après le dernier mot, l'utilisateur voit apparaître une boîte de dialogue avec le score final.
     */
    private fun onSkipWord() {
        if (viewModel.nextWord()) {
            setErrorTextField(false)
        } else {
            showFinalScoreDialog()
        }
    }

    /*
     * Creates and shows an AlertDialog with final score.
     * Crée et affiche un AlertDialog avec le score final.
     */
    private fun showFinalScoreDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.congratulations))
            .setMessage(getString(R.string.you_scored, viewModel.score.value))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.exit)) { _, _ ->
                exitGame()
            }
            .setPositiveButton(getString(R.string.play_again)) { _, _ ->
                restartGame()
            }
            .show()
    }

    /*
     * Re-initializes the data in the ViewModel and updates the views with the new data, to
     * restart the game.
     * Réinitialise les données dans le ViewModel et met à jour les vues avec les nouvelles données, pour
     * redémarrer le jeu.
     */
    private fun restartGame() {
        viewModel.reinitializeData()
        setErrorTextField(false)
    }

    /*
     * Exits the game.
     *  Quitte le jeu.
     */
    private fun exitGame() {
        activity?.finish()
    }

    /*
    * Sets and resets the text field error status.
    * Définit et réinitialise l'état d'erreur du champ de texte.
    */
    private fun setErrorTextField(error: Boolean) {
        if (error) {
            binding.textField.isErrorEnabled = true
            binding.textField.error = getString(R.string.try_again)
        } else {
            binding.textField.isErrorEnabled = false
            binding.textInputEditText.text = null
        }
    }
}